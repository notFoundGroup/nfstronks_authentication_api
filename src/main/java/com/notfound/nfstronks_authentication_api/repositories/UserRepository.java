package com.notfound.nfstronks_authentication_api.repositories;

import com.notfound.nfstronks_authentication_api.models.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findByEmail(String email);
}
