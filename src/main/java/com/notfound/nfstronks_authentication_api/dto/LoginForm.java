package com.notfound.nfstronks_authentication_api.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
public class LoginForm {
    @NotEmpty
    @Email
    String email;

    @NotEmpty
    String password;
}