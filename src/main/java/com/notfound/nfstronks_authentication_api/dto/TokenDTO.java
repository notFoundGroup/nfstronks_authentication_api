package com.notfound.nfstronks_authentication_api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TokenDTO {
    private String token;

    public TokenDTO(String token) {
        this.token = token;
    }
}
