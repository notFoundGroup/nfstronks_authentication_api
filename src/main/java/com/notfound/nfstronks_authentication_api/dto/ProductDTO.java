package com.notfound.nfstronks_authentication_api.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;


@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
public class ProductDTO {

    private Double id;

    @NotEmpty
    private String title;

    @NotEmpty
    private String author;

    @NotEmpty
    private String description;

    @NotEmpty
    private String country;

    @NotNull
    private Date launchDate;

    @NotNull
    private Boolean isAvailable;

    @NotNull
    private Double price;
}
