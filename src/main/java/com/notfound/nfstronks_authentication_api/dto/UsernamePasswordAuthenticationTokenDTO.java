package com.notfound.nfstronks_authentication_api.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class UsernamePasswordAuthenticationTokenDTO {
    Object principal;
    Object credentials;
    String[] rolesString;
}
