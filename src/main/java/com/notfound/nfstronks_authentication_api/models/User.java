package com.notfound.nfstronks_authentication_api.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tb_users")
@Getter
@Setter
@RequiredArgsConstructor
public class User {

    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_name", nullable = false, length = 200)
    private String name;

    @Column(name = "user_birthdate", nullable = false)
    private Date birthDate;

    @Column(name = "user_cpf", nullable = false, length = 11)
    private String cpf;

    @Column(name = "user_email", nullable = false, length = 11)
    private String email;

    @Column(name = "user_phone", nullable = false, length = 150)
    private String phone;

    @Column(name = "user_role", nullable = false, length = 5)
    private String role;

    @Column(name = "user_password", nullable = false)
    private String password;

}
