package com.notfound.nfstronks_authentication_api.controllers;


import com.notfound.nfstronks_authentication_api.dto.ProductDTO;
import com.notfound.nfstronks_authentication_api.exceptions.custom_exceptions.*;
import com.notfound.nfstronks_authentication_api.services.interfaces.ProductTokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import static com.notfound.nfstronks_authentication_api.util.ProjectUtils.stackTracingToLog;

@RestController
@Slf4j
@RequestMapping("/auth")
@CrossOrigin
public class ProductTokenController {

    private final ProductTokenService productTokenService;

    public ProductTokenController(ProductTokenService productTokenService) {
        this.productTokenService = productTokenService;
    }

    @PostMapping("/verifyproducttoken/{token}")
    ProductDTO createToken(@PathVariable String token) {
        try {
            return productTokenService.verifyToken(token);
        } catch (InvalidProductToken e) {
            throw new AuthException(e.getMessage());
        } catch (RequestNotFoundException e) {
            throw new BadRequestException(e.getMessage());
        } catch (Exception e) {
            stackTracingToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }
}
