package com.notfound.nfstronks_authentication_api.controllers;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.notfound.nfstronks_authentication_api.dto.LoginForm;
import com.notfound.nfstronks_authentication_api.dto.TokenDTO;
import com.notfound.nfstronks_authentication_api.dto.UsernamePasswordAuthenticationTokenDTO;
import com.notfound.nfstronks_authentication_api.exceptions.custom_exceptions.AuthException;
import com.notfound.nfstronks_authentication_api.exceptions.custom_exceptions.IncorrectPasswordExeption;
import com.notfound.nfstronks_authentication_api.exceptions.custom_exceptions.InternalServerErrorException;
import com.notfound.nfstronks_authentication_api.repositories.UserRepository;
import com.notfound.nfstronks_authentication_api.services.interfaces.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.notfound.nfstronks_authentication_api.util.ProjectUtils.stackTracingToLog;


@RestController
@Slf4j
@CrossOrigin
@RequestMapping("/auth")
public class AuthController {
    private final UserService userService;
    @Autowired
    UserRepository repository;

    public AuthController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/login")
    public ResponseEntity<TokenDTO> getToken(@Validated @RequestBody LoginForm loginForm) {
        try {
            return ResponseEntity.status(201).body(userService.createToken(loginForm));
        } catch (UsernameNotFoundException | IncorrectPasswordExeption e) {
            throw new AuthException(e.getMessage());
        } catch (Exception e) {
            stackTracingToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @PostMapping("/authorize")
    public ResponseEntity<UsernamePasswordAuthenticationTokenDTO> authorize(@RequestBody TokenDTO tokenDTO) {
        try {
            return ResponseEntity.ok(userService.authorizeToken(tokenDTO));

        } catch (JWTVerificationException e) {
            log.info(e.getMessage());
            throw new AuthException(e.getMessage());
        } catch (Exception e) {
            stackTracingToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @GetMapping("")
    public List<com.notfound.nfstronks_authentication_api.models.User> getall() {
        try {
            return (List<com.notfound.nfstronks_authentication_api.models.User>) repository.findAll();
        } catch (Exception e) {
            stackTracingToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }

    }
}

