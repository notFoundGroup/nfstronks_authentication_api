package com.notfound.nfstronks_authentication_api.services.interfaces;

import com.notfound.nfstronks_authentication_api.dto.ProductDTO;
import com.notfound.nfstronks_authentication_api.exceptions.custom_exceptions.InvalidProductToken;
import com.notfound.nfstronks_authentication_api.exceptions.custom_exceptions.RequestNotFoundException;

public interface ProductTokenService {

    ProductDTO verifyToken(String token) throws InvalidProductToken, RequestNotFoundException;

    public ProductDTO productByIdRequest(Long productID) throws RequestNotFoundException;
}
