package com.notfound.nfstronks_authentication_api.services.interfaces;

import com.notfound.nfstronks_authentication_api.dto.LoginForm;
import com.notfound.nfstronks_authentication_api.dto.TokenDTO;
import com.notfound.nfstronks_authentication_api.dto.UsernamePasswordAuthenticationTokenDTO;
import com.notfound.nfstronks_authentication_api.exceptions.custom_exceptions.IncorrectPasswordExeption;
import com.notfound.nfstronks_authentication_api.exceptions.custom_exceptions.UserNotFoundException;
import org.springframework.security.core.userdetails.UserDetailsService;


public interface UserService extends UserDetailsService {
    com.notfound.nfstronks_authentication_api.models.User findByEmail(String email) throws UserNotFoundException;

    TokenDTO createToken(LoginForm loginForm) throws IncorrectPasswordExeption;

    UsernamePasswordAuthenticationTokenDTO authorizeToken(TokenDTO tokenDTO) throws UserNotFoundException;
}
