package com.notfound.nfstronks_authentication_api.services.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.notfound.nfstronks_authentication_api.dto.LoginForm;
import com.notfound.nfstronks_authentication_api.dto.TokenDTO;
import com.notfound.nfstronks_authentication_api.dto.UsernamePasswordAuthenticationTokenDTO;
import com.notfound.nfstronks_authentication_api.exceptions.custom_exceptions.IncorrectPasswordExeption;
import com.notfound.nfstronks_authentication_api.exceptions.custom_exceptions.UserNotFoundException;
import com.notfound.nfstronks_authentication_api.models.User;
import com.notfound.nfstronks_authentication_api.repositories.UserRepository;
import com.notfound.nfstronks_authentication_api.services.interfaces.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.notfound.nfstronks_authentication_api.util.ProjectUtils.*;

@Service
@Primary
@Slf4j
public class UserServiceImpl implements UserService {

    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        log.info("Searching username with email: " + email);
        Optional<User> userOptional = userRepository.findByEmail(email);

        if (userOptional.isEmpty()) {
            throw new UsernameNotFoundException("Email/senha incorretos.");
        }

        User user = userOptional.get();

        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        log.info("Creating GrantedAuthority - " + user.getRole());
        authorities.add(new SimpleGrantedAuthority(user.getRole()));

        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), authorities);
    }

    @Override
    public User findByEmail(String email) throws UserNotFoundException {
        Optional<User> userOptional = userRepository.findByEmail(email);

        if (userOptional.isEmpty()) {
            throw new UserNotFoundException();
        }

        return userOptional.get();
    }

    @Override
    public TokenDTO createToken(LoginForm loginForm) throws IncorrectPasswordExeption {
        org.springframework.security.core.userdetails.User user =
                (org.springframework.security.core.userdetails.User) this.loadUserByUsername(loginForm.getEmail());

        log.info("Verifying password...");

        if (!bCryptPasswordEncoder.matches(loginForm.getPassword(), user.getPassword())) {
            throw new IncorrectPasswordExeption();
        }

        Algorithm algorithm = getAlgorithm(System.getenv("TOKEN_SECRET"));

        log.info("Creating token...");

        String accessToken = JWT.create()
                .withSubject(loginForm.getEmail())
                .withClaim("roles", user.getAuthorities()
                        .stream()
                        .map(GrantedAuthority::getAuthority)
                        .collect(Collectors.toList()))
                .withExpiresAt(new Date(System.currentTimeMillis() + HOURS * 10))
                .withIssuer(ISSUER)
                .sign(algorithm);


        return new TokenDTO(ATRIBUTO_PREFIXO + accessToken);
    }

    @Override
    public UsernamePasswordAuthenticationTokenDTO authorizeToken(TokenDTO tokenDTO) throws UserNotFoundException {
        String token = tokenDTO.getToken().replace(ATRIBUTO_PREFIXO, "");

        log.info("Authorazing Token: " + token);
        Algorithm algorithm = getAlgorithm(System.getenv("TOKEN_SECRET"));

        JWTVerifier verifier = JWT.require(algorithm).build();

        DecodedJWT decodedJWT = verifier.verify(token);

        String email = decodedJWT.getSubject();

        log.info("Getting roles...");
        String[] rolesString = decodedJWT.getClaim("roles").asArray(String.class);

        log.info("Creating AuthTokenDTO...");
        UsernamePasswordAuthenticationTokenDTO usernamePasswordAuthenticationAdapter =
                new UsernamePasswordAuthenticationTokenDTO(
                        this.findByEmail(email).getName(),
                        this.findByEmail(email).getId(),
                        rolesString
                );

        log.info(rolesString[0]);

        log.info("AuthenticationToken created: " + usernamePasswordAuthenticationAdapter.toString());

        return usernamePasswordAuthenticationAdapter;
    }

}
