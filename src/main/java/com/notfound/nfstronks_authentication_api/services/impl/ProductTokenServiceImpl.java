package com.notfound.nfstronks_authentication_api.services.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.notfound.nfstronks_authentication_api.dto.ProductDTO;
import com.notfound.nfstronks_authentication_api.exceptions.custom_exceptions.InvalidProductToken;
import com.notfound.nfstronks_authentication_api.exceptions.custom_exceptions.RequestNotFoundException;
import com.notfound.nfstronks_authentication_api.services.interfaces.ProductTokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

import static com.notfound.nfstronks_authentication_api.util.ProjectUtils.getAlgorithm;

@Slf4j
@Service
public class ProductTokenServiceImpl implements ProductTokenService {

    @Override
    public ProductDTO verifyToken(String token) throws InvalidProductToken, RequestNotFoundException {
        try {
            Algorithm algorithm = getAlgorithm(System.getenv("TOKEN_SECRET"));

            JWTVerifier verifier = JWT.require(algorithm).build();

            log.info("Verifying token secret");
            DecodedJWT decodedJWT = verifier.verify(token);

            String productID = decodedJWT.getSubject();

            ProductDTO product = productByIdRequest(Long.parseLong(productID));

            return product;

        } catch (JWTVerificationException e) {
            throw new InvalidProductToken();
        }
    }

    @Override
    public ProductDTO productByIdRequest(Long productID) throws RequestNotFoundException {

        try {
            URI uri = new URI(System.getenv("API_PRODUCT_URL") + "/" + productID);

            log.info(String.valueOf(uri) + " -------- URI-Product");

            RequestEntity<ProductDTO> requestEntityProduct = new RequestEntity<>(HttpMethod.GET, uri);

            log.info("Starting request with productID: " + productID);

            ResponseEntity<ProductDTO> productDTOResponseEntity = new RestTemplate().exchange(requestEntityProduct,
                    ProductDTO.class);


            log.info(productDTOResponseEntity.getBody().getTitle() + " -- Produto");

            return productDTOResponseEntity.getBody();

        } catch (URISyntaxException e) {
            throw new RequestNotFoundException(e.getReason());
        } catch (RestClientException e) {
            throw new RequestNotFoundException(e.getMessage());
        }
    }
}
