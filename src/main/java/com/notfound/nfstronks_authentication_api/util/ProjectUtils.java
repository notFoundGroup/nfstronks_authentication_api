package com.notfound.nfstronks_authentication_api.util;

import com.auth0.jwt.algorithms.Algorithm;
import lombok.extern.slf4j.Slf4j;

import java.io.PrintWriter;
import java.io.StringWriter;

@Slf4j
public class ProjectUtils {
    public static final int MILI_SECS = 1;
    public static final int SECS = MILI_SECS * 1000;
    public static final int MINUTES = SECS * 60;
    public static final int HOURS = MINUTES * 60;
    public static final String HEADER_ATRIBUTO = "Authorization";
    public static final String ATRIBUTO_PREFIXO = "Bearer ";
    public static final String ISSUER = "Admin";
    public static final String TOKEN_SENHA = "ULTRASECRETAPRANAOROUPAMEUDADOS=(";

    public static Algorithm getAlgorithm(String secret) {
        return Algorithm.HMAC256(secret.getBytes());
    }

    public static void stackTracingToLog(Exception e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String stackTraceAsString = sw.toString();
        log.error(stackTraceAsString);
    }
}

