package com.notfound.nfstronks_authentication_api.exceptions.custom_exceptions;

public class InvalidProductToken extends Exception {

    public InvalidProductToken() {
        super("Product token is invalid");
    }
}
