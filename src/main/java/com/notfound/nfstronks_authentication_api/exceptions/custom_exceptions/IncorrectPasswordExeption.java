package com.notfound.nfstronks_authentication_api.exceptions.custom_exceptions;

public class IncorrectPasswordExeption extends Exception {
    public IncorrectPasswordExeption() {
        super("Email/senha incorretos.");
    }
}
