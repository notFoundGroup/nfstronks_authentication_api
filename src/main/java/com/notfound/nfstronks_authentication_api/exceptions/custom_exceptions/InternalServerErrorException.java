package com.notfound.nfstronks_authentication_api.exceptions.custom_exceptions;

public class InternalServerErrorException extends RuntimeException {

    public InternalServerErrorException(String message) {
        super(message);
    }
}
