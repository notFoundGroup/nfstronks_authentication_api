package com.notfound.nfstronks_authentication_api.exceptions.custom_exceptions;

public class UserNotFoundException extends Exception {
    public UserNotFoundException() {
        super("Usuario não encontrado!");
    }
}
