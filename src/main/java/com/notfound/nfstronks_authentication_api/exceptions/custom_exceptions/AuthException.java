package com.notfound.nfstronks_authentication_api.exceptions.custom_exceptions;

import lombok.Builder;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Builder
public class AuthException extends RuntimeException {
    public AuthException(String message) {
        super(message);
    }
}
