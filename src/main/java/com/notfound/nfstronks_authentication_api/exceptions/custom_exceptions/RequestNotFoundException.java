package com.notfound.nfstronks_authentication_api.exceptions.custom_exceptions;

public class RequestNotFoundException extends Exception {
    public RequestNotFoundException(String message) {
        super(message);
    }
}
