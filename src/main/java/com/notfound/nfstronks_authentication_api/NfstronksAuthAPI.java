package com.notfound.nfstronks_authentication_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NfstronksAuthAPI {
    public static void main(String[] args) {
        SpringApplication.run(NfstronksAuthAPI.class, args);
    }
}
