FROM openjdk:11

WORKDIR /app

COPY target/nfstronks_authentication_api-1.jar /app/spring-app.jar

ENTRYPOINT ["java", "-jar", "spring-app.jar"]

EXPOSE 8080